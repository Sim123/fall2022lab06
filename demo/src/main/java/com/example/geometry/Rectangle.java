package com.example.geometry;

public class Rectangle implements Shape {
    double width;
    double lenght;
    
    public Rectangle(double width, double lenght) {
        this.width = width;
        this.lenght = lenght;
    }
    public double getHeight() {
        return this.width;
    }
    public double getLenght() {
        return this.lenght;
    }
    public double getArea() {
        return this.width * this.lenght;
    }
    public double getPerimeter() {
        return (2 * this.width) + (2 * this.lenght);
    }
}
