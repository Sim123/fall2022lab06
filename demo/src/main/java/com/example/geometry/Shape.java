package com.example.geometry;

interface Shape {
    double getArea();
    double getPerimeter();
}
