package com.example.geometry;

public class LotsOfShapes {
    public static void main(String[] args){
        Shape s[] = new Shape[5];
        s[0] = new Rectangle(3,2);
        s[1] = new Rectangle(8, 9);
        s[2] = new Circle(7);
        s[3] = new Circle(5);
        s[4] = new Square(7);

        for (int i = 0; i < s.length; i++) {
            System.out.println(s[i].getArea());
            System.out.println(s[i].getPerimeter());
        }
    }
}
